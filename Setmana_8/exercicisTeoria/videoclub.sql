/** EXERCICI 1 **/
/**
    Llista de dates i imports de les factures, junt amb el nom i el telèfon del client associat.
**/
SELECT videoclub.client.Nom, videoclub.client.Telefon,
videoclub.factura.Data, videoclub.factura.Import
FROM videoclub.factura, videoclub.client
WHERE videoclub.factura.DNI = videoclub.client.DNI;