/** EXERCICI 1 **/
/**
Ordenació de files
Obteniu un llistat dels pacients (cognom, nom) assignats a la planta 2 ordenada primer per cognom i després per nom en ordre descendent. 
**/
SELECT cognoms  AS  'cognom',  nom AS 'nom'
FROM hospital.pacient
WHERE numP = 2
ORDER BY cognoms DESC ,  nom  DESC;

/** EXERCICI 2 **/
/**
Excloure files repetides
Obteniu un llistat de les àrees (numArea) on hi ha infermers assignats. 
**/
SELECT DISTINCT numArea AS 'numArea'
FROM hospital.infermer;

/** EXERCICI 3 **/
/**
Agrupament de files
Obteniu un llistat de les àrees amb la quantitat de metges a cada àrea. Atenció que hi ha un metge que no està assignat a cap àrea. 
**/
SELECT numArea  'numArea',   COUNT(DISTINCT  codMetge)  'QuantitatMetges'
FROM  hospital.metge
WHERE  numArea  >  0
GROUP BY  numArea;

/** EXERCICI 4 **/
/**
Agrupament de files
Obteniu un llistat de les plantes amb la quantitat de pacients a cada planta. 
**/
SELECT numP  'numP',  COUNT(numP)  'QuantitatPacients'
FROM hospital.pacient
WHERE dni > 0
GROUP BY numP;

/** EXERCICI 5 **/
/**
Agrupament de files
Obteniu un llistat de les àrees on aparegui el sou màxim, el sou mínim, el sou mitjà i la suma dels sous dels metges assignats a cada àrea. Atenció que hi ha un metge que no està assignat a cap àrea.
**/
SELECT numArea  'numArea',  MAX(sou)  'SouMax',  MIN(sou)  'SouMin',
ROUND( AVG( sou ) ) 'SouAvg',  SUM(sou)  'Sumsou'
FROM hospital.metge
WHERE numArea > 0
GROUP BY numArea;

/** EXERCICI 6 **/
/**
Agrupament de files 
Obteniu un llistat de les plantes (número planta i nombre pacients) on hi hagi més de 4 pacients. 
**/
SELECT numP 'numP',  COUNT(dni)  'COUNT*'
FROM hospital.pacient
GROUP BY numP
HAVING COUNT(dni) > 4;


/** EXERCICI 7 **/
/**
Unió, intersecció i diferència de sentències select
Obteniu un llistat (codi, nom i cognoms) de tot el personal de l'hospital (metges i infermers).
**/
SELECT codMetge 'codi',  nom  'nom',  cognoms 'cognoms'
FROM hospital.metge

UNION

SELECT codInf  'codi',  nom  'nom',  cognoms  'cognoms'
FROM hospital.infermer;

/** EXERCICI 8 **/
/**
Unió, intersecció i diferència de sentències select
Obteniu un llistat (codi, nom i cognoms) de tot el personal de l'hospital (metges i infermers).
**/
SELECT codMetge 'codi',  nom  'nom',  cognoms 'cognoms'
FROM hospital.metge

UNION

SELECT codInf  'codi',  nom  'nom',  cognoms  'cognoms'
FROM hospital.infermer;


/** EXERCICI 9 **/
/**
Combinacions entre taules
Obteniu un llistat dels metges amb la informació de l'àrea on pertanyen. 
**/

SELECT codMetge, nom, cognoms, sou, hospital.area.numArea, hospital.area.nomArea
FROM hospital.metge,  hospital.area
WHERE hospital.metge.numArea = hospital.area.numArea


/** EXERCICI 10 **/
/**
Combinacions entre taules
Obteniu un llistat de les visites afegint el nom i cognoms del pacient realitzades pel metge Carles Vila ordenada per cognoms i nom.
**/
USE hospital;
SELECT p.nom,  p.cognoms,  v.data,  v.diagnostic
FROM pacient p INNER JOIN visita v
ON p.dni = v.dniPac
WHERE v.codMetge=(	SELECT codMetge
			FROM metge
			WHERE UPPER(nom) = 'CARLES' AND UPPER(cognoms) = 'VILA'
		)
ORDER BY p.cognoms, p.nom

/** EXERCICI 11 **/
/**
Combinacions entre taules
Obteniu un llistat dels metges amb la informació de l'àrea on treballen. La consulta ha de mostrar tots els metges encara que no estiguin assignats a cap àrea. 
**/
USE hospital;
SELECT m.codMetge, m.nom, m.cognoms, m.sou, m.numArea, a.nomArea
FROM metge m LEFT JOIN area  a
ON m.numArea = a.numArea
ORDER BY m.codMetge;

/** EXERCICI 12 **/
/**
Subconsultes
Obteniu un llistat dels metges que cobren més que el Carles Vila. 
**/
USE hospital;
SELECT coalesce(codMetge), nom, cognoms, sou, numArea
FROM metge
WHERE sou > (	SELECT sou
		FROM metge
		WHERE UPPER(nom) = 'CARLES' AND UPPER(cognoms) = 'VILA'
	)

/** EXERCICI 13 **/
/**
Subconsultes
Obteniu un llistat dels metges que cobren més que la mitjana de l'hospital. 
**/
USE hospital;
SELECT coalesce(codMetge), nom, cognoms, sou, numArea
FROM metge
WHERE sou > (	SELECT AVG(sou)
				FROM metge
			)