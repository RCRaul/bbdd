/**** ELS ESQUEMES SON ELS MATEIXOS QUE A LA SETMANA 7 ***/

/** EXERCICI 1 **/
/**
    Obteniu un llistat dels metges (nom, cognoms i sou).
    El sou emmagatzemat és l'anual i ha de sortir el mensual arrodonit a dos decimals
    (dividit per 14 ja que hi ha dues pagues extres). 
**/
SELECT nom, cognoms,
(ROUND (sou / 14) ) AS "souMensual"
FROM hospital.metge;

/** EXERCICI 2 **/
/**
    Obteniu un llistat dels metges (nom i cognom) 
    on aparegui el nom complet com cognom, nom tal com surt al resultat i en majúscules.
    SORTIDA (una unica columna):
        Cognoms_Nom
    'MONTAGUT, ALBERT'
            ....
**/
SELECT  UPPER ( concat (metge.cognoms, ", " , metge.nom ) ) AS "Cognoms_Nom"
FROM hospital.metge;

/** EXERCICI 3 **/
/**
    Obteniu un llistat de les visites programades en dilluns.
    La consulta ha de mostrar també que són en Dilluns o bé Monday (més fàcil).
**/

/** DIA EN ANGLES **/
SELECT dniPac, codMetge, data, diagnostic,
DAYNAME (data)  AS   "dia_Setmana"
FROM  hospital.visita
WHERE DAYNAME (data)  IN  ('Monday')
ORDER BY data;

/** DIA EN ANGLES 2 **/
SELECT dniPac, codMetge, data, diagnostic,
DAYNAME (data)  AS   "dia_Setmana"
FROM  hospital.visita
WHERE DAYNAME (data)  LIKE 'monday'
ORDER BY data;

/** DIA EN CATALA **/
SELECT dniPac, codMetge, data, diagnostic,
CASE
    WHEN WEEKDAY(data)=0 THEN 'Dilluns'
    WHEN WEEKDAY(data)=1 THEN 'Dimarts'
    WHEN WEEKDAY(data)=2 THEN 'Dimecres'
    WHEN WEEKDAY(data)=3 THEN 'Dijous'
    WHEN WEEKDAY(data)=4 THEN 'Divendres'
    WHEN WEEKDAY(data)=5 THEN 'Dissabte'
    ELSE 'Diumenge'
END AS "Dia_Setmana"
FROM hospital.visita
WHERE WEEKDAY(data)= 0
ORDER BY data;

/** EXERCICI 4 **/
/**
    Obteniu un llistat de les visites de l'1 al 15 d'abril de 2014 ambdós inclosos on la data ha de sortir tal com s'indica al resultat.
    SORTIDA DE LA DATA:
    02/April/2014
**/
SELECT  dniPac,  codMetge,
DATE_FORMAT (data, '%d/%M/%Y' ) AS  'diagnostic'
FROM hospital.visita
WHERE data BETWEEN('2014-03-31') AND ('2014-04-16')
ORDER BY data;