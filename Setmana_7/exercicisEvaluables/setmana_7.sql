/** EXERCICI 1 **/
/**
    Especifiqueu el codi SQL que permet obtenir
    un llistat de tots els metges, a partir del qual obtindreu aquest resultat......
**/
SELECT  coalesce(codMetge) AS 'codMetge',
        nom, cognoms, sou, numArea
FROM hospital.metge


/** EXERCICI 2 **/
/**
    Especifiqueu el codi SQL que permet obtenir un llistat de les visites realitzades entre l'1 de març del 2014 i el 10 de març del 2014 (ambdós inclosos), a partir del qual obtindreu aquest resultat......
**/
SELECT coalesce(dniPac) AS 'dniPac',  codMetge, data, diagnostic
FROM hospital.visita
WHERE data BETWEEN ('2014-02-28') AND ('2014-03-10')
ORDER BY data;

/** EXERCICI  3**/
/**
    Especifiqueu el codi SQL que permet obtenir un llistat dels metges (totes les dades) no assignats a cap àrea, a partir del qual obtindreu aquest resultat......
 **/

 SELECT coalesce(codMetge) AS 'codMetge',
        nom, cognoms, sou, numArea
FROM hospital.metge
WHERE numArea IS NULL;

/** EXERCICI  4**/
/**
    Especifiqueu el codi SQL que permet obtenir un llistat dels metges (totes les dades) assignats a les àrees 1, 3 o 5, a partir del qual obtindreu aquest resultat......
**/
SELECT coalesce(codMetge) AS 'codMetge', 
		nom, cognoms, sou, numArea
FROM hospital.metge
WHERE numArea IN(1,3,5);

/** EXERCICI  5**/
/**
    Especifiqueu el codi SQL que permet obtenir un llistat de tots els metges assignats a l'àrea 1, a partir del qual obtindreu aquest resultat......
**/
SELECT coalesce(codMetge) AS 'codMetge',
        nom, cognoms, sou, numArea
FROM hospital.metge
WHERE numArea IN(1);

/** EXERCICI  6**/
/**
    Especifiqueu el codi SQL que permet obtenir un llistat dels pacients que es troben a la planta 2, a partir del qual obtindreu aquest resultat......
**/
SELECT coalesce(dni) AS 'dni',
        nom, cognoms, dataAlta, numP, numL
FROM hospital.pacient
WHERE numP IN(2);

/** EXERCICI  7**/
/**
    Especifiqueu el codi SQL que permet obtenir les dades del pacient al qual se li hagi assignat el llit 1 de la planta 1, a partir del qual obtindreu aquest resultat......
**/
SELECT coalesce(dni) AS 'dni',
        nom, cognoms, dataAlta, numP, numL
FROM hospital.pacient
WHERE numP IN(1) AND numL IN(1);

/** EXERCICI  8**/
/**
    Especifiqueu el codi SQL que permet obtenir un llistat dels metges (nom, cognoms i sou). El sou emmagatzemat és l'anual i ha de sortir el mensual (dividit per 14 ja que hi ha dues pagues extres), a partir del qual obtindreu aquest resultat......
**/
SELECT coalesce(codMetge) AS 'codMetge',
        nom, cognoms, (sou/14)
FROM hospital.metge
ORDER BY sou;
/** EXERCICI  9**/
/**
    Especifiqueu el codi SQL que permet obtenir un llistat dels metges (nom, cognoms i sou) mostrant el sou mensual (dividit per 14) i definit un àlies per la columna sou, a partir del qual obtindreu aquest resultat......
**/
SELECT coalesce(codMetge) AS 'codMetge',
        nom, cognoms, (sou/14) AS 'sou_Mensual'
FROM hospital.metge
ORDER BY sou;

/** EXERCICI  10**/
/**
    Especifiqueu el codi SQL que permet obtenir un llistat de tots els pacients (nom, cognom i dataAlta) assignats a la planta 2 i ordenats per nom, a partir del qual obtindreu aquest resultat......
**/
SELECT coalesce(nom) AS 'nom', cognoms, dataAlta
FROM hospital.pacient
WHERE numP IN(2)
ORDER BY nom;

/** EXERCICI  11**/
/**
    Especifiqueu el codi SQL que permet obtenir un llistat de les pacients (totes les dades) amb el nom començant per 'A', a partir del qual obtindreu aquest resultat......
**/
SELECT coalesce(dni) AS 'dni', nom, cognoms, dataAlta, numP, numL
FROM hospital.pacient
WHERE UPPER(nom) LIKE ( 'A%' )
ORDER BY nom;

/** EXERCICI  12**/
/**
     Els dos caràcters fonamentals dels patrons...

Especifiqueu el codi SQL que permet obtenir un llistat dels pacients (totes les dades) amb el nom que tingui com a segona lletra la 'a', a partir del qual obtindreu aquest resultat......
**/
SELECT coalesce(dni) AS 'dni', nom, cognoms, dataAlta, numP, numL
FROM hospital.pacient
WHERE UPPER(nom) LIKE ( '_A%' )
ORDER BY nom;




