/* CONSULTAS SIMPLES EN L'ESQUEMA SANITAT*/

/*
1. Mostreu els hospitals existents (número, nom i telèfon). 
2. Mostreu els hospitals existents (número, nom i telèfon) que tinguin una lletra A en la segona posició del nom. 
3. Mostreu els treballadors (codi hospital, codi sala, número empleat i cognom) existents. 
4. Mostreu els treballadors (codi hospital, codi sala, número empleat i cognom) que no siguin del torn de nit.
5. Mostreu els malalts nascuts l’any 1960. 
6. Mostreu els malalts nascuts a partir de l’any 1960.  
*/

/* 1. Mostreu els hospitals existents (número, nom i telèfon).  */
SELECT HOSPITAL_COD AS "Número", NOM AS "Nom", TELEFON AS "Telèfon"
FROM sanitat.hospital;

/* 2. Mostreu els hospitals existents (número, nom i telèfon) que tinguin una lletra A en la segona posició del nom.   */
SELECT HOSPITAL_COD AS "Número", NOM AS "Nom", TELEFON AS "Telèfon"
FROM sanitat.hospital
WHERE LOWER(NOM) LIKE '_a%'; 

/* 3. Mostreu els treballadors (codi hospital, codi sala, número empleat i cognom) existents.   */


/* 4. Mostreu els treballadors (codi hospital, codi sala, número empleat i cognom) que no siguin del torn de nit.  */


/* 5. Mostreu els malalts nascuts l’any 1960.  */


/* 6. Mostreu els malalts nascuts a partir de l’any 1960.  */