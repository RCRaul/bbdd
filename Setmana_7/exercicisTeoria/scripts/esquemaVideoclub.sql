/* CONSULTAS SIMPLES EN L'ESQUEMA VIDEOCLUB*/

/*
1. Llista de noms i telèfons dels clients.
2. Llista de dates i imports de les factures.
3. Llista de productes (descripció) facturats en la factura número 3.
4. Llista de factures ordenada de forma decreixent per import
5. Llista dels actors el nom dels quals comenci per X.
*/

/* 1. Llista de noms i telèfons dels clients. */
SELECT Nom, Telefon
FROM videoclub.client;

/* 2. Llista de dates i imports de les factures.*/ 
SELECT Data, Import
FROM videoclub.factura;

/* 3. Llista de productes (descripció) facturats en la factura número 3. */
SELECT Descripcio
FROM videoclub.detallfactura
WHERE CodiFactura = 3;

/* 4. Llista de factures ordenada de forma decreixent per import */
SELECT *
FROM videoclub.factura
ORDER BY Import DESC;

/* 5. Llista dels actors el nom dels quals comenci per X. */
SELECT Nom
FROM videoclub.actor
WHERE UPPER(Nom) LIKE 'X%';