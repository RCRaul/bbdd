/* CONSULTAS SIMPLES EN L'ESQUEMA EMPRESA*/

/*
1. Mostreu els productes (codi i descripció) que comercialitza l’empresa.
2. Mostreu els productes (codi i descripció) que contenen la paraula tennis en la descripció.
3. Mostreu el codi, nom, àrea i telèfon dels clients de l’empresa.
4. Mostreu els clients (codi, nom, ciutat) que no són de l’àrea telefònica 636.
5. Mostreu les ordres de compra de la taula de comandes (codi, dates d’ordre i de tramesa). 
*/

/* 1. Mostreu els productes (codi i descripció) que comercialitza l’empresa. */
SELECT PROD_NUM AS "Codi", DESCRIPCIO AS "Descripcio"
FROM empresa.producte;

/* 2. Mostreu els productes (codi i descripció) que contenen la paraula tennis en la descripció. */
SELECT PROD_NUM AS "Codi", DESCRIPCIO AS "Descripcio"
FROM empresa.producte
WHERE UPPER(DESCRIPCIO) LIKE '%TENNIS%';

/* 3. Mostreu el codi, nom, àrea i telèfon dels clients de l’empresa. */
SELECT CLIENT_COD AS "Codi", NOM AS "Nom", AREA AS "Àrea", TELEFON AS "Telèfon"
FROM empresa.client;

/* 4. Mostreu els clients (codi, nom, ciutat) que no són de l’àrea telefònica 636. */
SELECT CLIENT_COD AS "Codi", NOM AS "Nom", CIUTAT AS "Ciutat"
FROM empresa.client
WHERE AREA = 636; /* funciona sense cometes és SMALLINT */

/* 4.BIS. Mostreu els clients (codi, nom, ciutat) que no són de l’àrea telefònica 636. */
SELECT CLIENT_COD AS "Codi", NOM AS "Nom", CIUTAT AS "Ciutat"
FROM empresa.client
WHERE area in(636);

/* 5. Mostreu les ordres de compra de la taula de comandes (codi, dates d’ordre i de tramesa).  */
SELECT COM_NUM AS "Codi", COM_DATA AS "Data Ordre", DATA_TRAMESA AS "Data Tramesa"
FROM empresa.comanda;
