/** Mostreu el nom i cognoms dels treballadors càmeres ordenats per cognoms i nom **/
USE videos;
SELECT s.Codi 'Codi', s.Nom 'Nom', CONCAT(s.Cog_1, ' ', s.Cog_2) 'Cognoms', Telefon 'Telèfon', p.Especialitat 'Especialitat'
FROM staff s INNER JOIN personal p
ON s.Codi = p.Codi
WHERE p.Especialitat LIKE 'Cà%'
ORDER BY Cognoms, Nom

/** Mostrar nom i cognoms dels actors, que cobren entre 300.000 i 500.000 euros **/
USE videos;
SELECT 	a.Codi 'Codi', s.Nom 'Nom', CONCAT(s.Cog_1, ' ', s.Cog_2) 'Cognoms',
		a.Sou 'Between 300M and 500M'
FROM actor a 
INNER JOIN staff s ON a.Codi = s.Codi
WHERE a.Sou BETWEEN 300000 AND 500000
ORDER BY s.Nom, s.Cog_1 ASC

/** Mostrar  totes les dades dels actors que cobren més de la mitja dels sous de tots els actors **/
USE videos;
SELECT	a.Codi 'Codi', s.Nom 'Nom', CONCAT(s.Cog_1, ' ', s.Cog_2) 'Cognoms', s.Telefon 'Telèfon',
		a.Sou 'Sou > Mitjana', t.CodiSerie 'Serie', t.Data_inici 'Inicia', t.Data_fi 'Final', t.Paper 'Rol', 		
        i.CodiInc 'Incompatible amb', i.Grau 'Grau'
FROM actor a 
INNER JOIN staff s ON a.Codi = s.Codi
INNER JOIN treballa t ON a.Codi = t.CodiActor
LEFT JOIN incompatible i ON a.Codi = i.CodiAct
WHERE a.Sou > ( SELECT AVG(Sou) FROM actor)

/** Mostreu el número de series que s’emeten a cada canal **/
USE videos;
select s.NomC 'Canal', COUNT(s.Codi) 'Nombre de Series'
FROM serie s
WHERE s.NomC NOT LIKE ' '
GROUP BY Canal
ORDER BY Canal

/** Mostreu el nom i cognoms dels actors que no treballen a cap serie **/
USE videos;
SELECT  s.Nom 'Nom', CONCAT(s.Cog_1,' ',s.Cog_2) 'Cognoms' 
FROM  staff s
INNER  JOIN  actor a ON a.Codi = s.Codi
WHERE  s.Codi NOT IN (SELECT  CodiActor FROM  treballa)

/** Mostreu les dades del director de més edat que ha guanyat algun premi **/
USE videos;
SELECT	s.Codi 'Codi Director',  s.Nom 'Nom', CONCAT(s.Cog_1,' ',s.Cog_2)'Cognoms', s.Telefon 'Telèfon',
		d.Data_naix 'Nascut el', TIMESTAMPDIFF(YEAR,d.Data_naix,CURDATE()) 'Edat',
        p.Codi 'Codi Premi', p.Nom 'Premi', p.Any 'Guanyat en'
FROM director d
INNER JOIN premi p ON d.Codi = p.CodiDir
INNER JOIN staff s ON d.Codi = s.Codi
WHERE d.Data_naix < (SELECT AVG(Data_naix) FROM director)


/** Mostreu el nom de la serie en la qual treballen més de dos actors **/
USE videos;
SELECT s.Nom, t.CodiSerie, COUNT(a.Codi) 'Actors que treballen'
FROM serie s
INNER JOIN treballa t ON s.Codi = t.CodiSerie
INNER JOIN actor a ON a.Codi = t.CodiActor
GROUP BY s.Nom
HAVING COUNT(a.Codi) > 1
ORDER BY s.Nom

/** ESQUEMA HOSPITAL **/

/** Mostreu el nom de les proves que s’han realitzat als pacients que han estat donats d’alta al març de 2014. 
No han de sortir dades repetides **/
USE hospital;
SELECT descr
FROM prova INNER JOIN realitzacio ON prova.codProv = realitzacio.codProv
			INNER JOIN pacient ON realitzacio.dniPac = pacient.dni
WHERE pacient.dataAlta BETWEEN ('2014-03-01') AND ('2014-03-30')
GROUP BY descr

/** Obteniu un llistat de les àrees on la suma dels sous de tots els seus metges no superi els 200.000€.
La consulta ha de mostrar també la suma dels sous **/
USE hospital;
SELECT area.numArea 'Cod. Area', area.nomArea 'Area', sum(metge.sou) 'Total Sous'
FROM area INNER JOIN metge ON area.numArea = metge.numArea
GROUP BY area.nomArea
HAVING sum(metge.sou) <= 200000
ORDER BY area.numArea

/** Mostreu el nom i cognom de cada pacient junt amb la suma dels sous dels metges que els han atés. **/
USE hospital;
SELECT CONCAT(pacient.nom,' ',pacient.cognoms) 'Pacient', sum(sou) 'Sous'
FROM metge INNER JOIN visita ON metge.codMetge = visita.codMetge
			INNER JOIN pacient ON pacient.dni = visita.dniPac
GROUP BY visita.dniPac
ORDER BY pacient.nom, pacient.cognoms
